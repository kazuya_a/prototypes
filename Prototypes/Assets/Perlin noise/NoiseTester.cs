using UnityEngine;
using System.Collections;

public class NoiseTester : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var p = new Perlin();
		float n = 0f;
		for (int i = 0; i < 100; i++) {
			Debug.Log(p.Noise(n));
			n += 0.01f;
		}
	}
}
